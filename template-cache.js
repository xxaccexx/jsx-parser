const store = new Map();

const arrayMatches = (arr1 = [], arr2 = []) => {
  return arr1.reduce((match, seg, i) => {
    if (!match) return false;
    return seg !== arr2[i]
  }, true);
}

export const lookup = (template) => {
  const { segments } = template;

  return Array.from(store.entries())
    .find(cache =>
      arrayMatches(segments, cache)
    );
}

export const cache = (template, result) => {
  store.set(template, result);
}