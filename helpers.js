import { inspect } from 'util';

const dumpObj = (obj) => inspect(obj, false, 10000, true);

export const dump = (...objs) => console.log(...objs.map(o => `${ dumpObj(o) }\n`));