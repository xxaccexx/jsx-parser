import { printError } from './src/parser/parse';

export const initialStateObject = {
	errorMsg: undefined,
	isError: false,
	index: 0,
	result: undefined,
}


export const defineTest = (pattern, targetString, index, result, errorMsg) => {
	const exec = pattern({
		...initialStateObject,
		targetString,
	})

	if (exec.isError) {
		try {
			console.log(
				printError(exec.targetString, exec.index)
			)
		}
		catch {
			console.log(JSON.stringify(exec, null, '  '))
		}
	}

	expect(
		exec
	).toEqual({
		...initialStateObject,
		targetString,
		index,
		result,
		errorMsg,
		isError: !!errorMsg,
	});
}