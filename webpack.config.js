const path = require('path')

module.exports = {
	mode: 'development',
	stats: 'errors-only',
	entry: './index.js',
	output: {
		path: path.resolve(process.cwd(), 'dist'),
		filename: 'index.js',
	},
};
