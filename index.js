import { parse as html } from './src'

html`<a attr attr="asd" attr=${123.456} />`
html`<a attr attr="asd" attr=${123.456}></a>`
html`<a attr attr="asd" attr=${123.456}>content</a>`
html`<a attr attr="asd" attr=${123.456}>content${123}asd</a>`

html`<a><a /></a>`

html`
	<span />
`

html`<p>something</p>`

html`<hello></hello>`

html`<hello>test</hello>`
html`<hello><hello></hello></hello>`
html`<hello />`
html`<Test><p>something</p><input /></Test>`
html`
	<h1>
		Heading text
		<span>
			${'Hello, World!'}
		</span>
		but on on that
		<button @click=${() => console.log()}>Increment</button>
	</h1>
`
