export const updateParserState = (state, index, result, isError) => ({
  ...state,
  index,
	result,
	isError: isError ?? state.isError		//	preserve previous isError, if it is set
});

export const updateParserIndex = (state, index) => ({
  ...state,
  index
});

export const updateParserResult = (state, result) => ({
  ...state,
  result
});

export const updateParserError = (state, errorMsg) => ({
  ...state,
  isError: true,
	errorMsg
});

export const removeParserError = (state) => {
	const { isError, errorMsg, ...rest } = state;
	return { ...rest, isError: false, errorMsg: undefined }
}

export const parserFn = fn => parserState => {
	if (parserState.isError) return parserState;
	return fn(parserState);
};