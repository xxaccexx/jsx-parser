import { parserFn, updateParserResult } from './state.js';

import {
	str,
	sequence,
	many,
	optional,
	choice,
} from './syn.js';

import {
	alphabet,
	alphaNumeric,
	lowercase,
	numbers,
} from './chars.js';

// `something` | `Something` -> 'something' | 'Something'
export const variableName = () => {
	const pattern = sequence(
		alphabet(),
		many(
			alphaNumeric()
		)
	);

	const parser = parserFn(parserState => {
		const result = pattern(parserState);
		if (result.isError) return result;

		const [ firstChar, rest ] = result.result;
		const isUppercase = firstChar.toLowerCase() != firstChar;

		return updateParserResult(
			result,
			{
				isUppercase,
				value: [ firstChar, ...rest ].join('')
			}
		)
	})

	parser.pattern = pattern.pattern;
	return parser;
}

//	`something` -> true, `some-thing` -> true, `Something` -> false
export const tagName = () => {
	const pattern = sequence(
		lowercase(),
		many(
			choice(
				numbers(),
				lowercase(),
				str('-')
			)
		)
	);

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;

		const [ first, rest ] = test.result;

		return updateParserResult(
			test,
			{
				type: 'tag',
				value: [ first, ...rest ].join('')
			}
		)
	})

	parser.pattern = pattern.pattern;
	return parser;
}

// `.Awesome` | `.awesome` -> 'Awesome' | 'awesome'
export const propertyName = () => {
	const pattern = sequence(
		str('.'),
		variableName()
	);
	
	const parser = parserFn(parserState => {
		const result = pattern(parserState);
		if (result.isError) return result;

		const [ dot, property ] = result.result;

		return updateParserResult(
			result,
			property
		)
	})

	parser.pattern = pattern.pattern;
	return parser;
}

// `something.Awesome` | `Something.awesome` -> ['something','Awesome'] | ['Something', 'awesome']
export const objectRef = () => {
	const pattern = sequence(
		variableName(),
		optional(many(propertyName()))
	);
	
	const parser = parserFn(parserState => {
		const result = pattern(parserState);
		if (result.isError) return result;

		const [ variable, properties = [] ] = result.result;

		const type = properties.length || variable.isUppercase ? 'component' : 'element';
		const name = [variable.value, ...properties.map(prop => prop.value) ].join('.');

		return updateParserResult(
			result,
			{
				type,
				name
			}
		);
	})

	parser.pattern = pattern.pattern;
	return parser;
}