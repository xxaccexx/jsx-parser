import {
	updateParserError,
	updateParserIndex,
	updateParserState,
	updateParserResult,
	parserFn,
} from './state.js';


export const str = pattern => {
	const parser = parserFn(parserState => {
		if (parserState.index >= parserState.targetString.length) {
			return updateParserError(
				parserState,
				'Unexpected EOF'
			)
		}

		const test = parserState.targetString
			.substr(
				parserState.index,
				pattern.length
			);

		if (test === pattern) {
			return updateParserState(
				parserState,
				parserState.index + pattern.length,
				test,
			)
		}

		return updateParserError(
			parserState,
			`Expected '${pattern}' and got '${test}' instead`
		)
	});

	parser.pattern = pattern;

	return parser;
}

export const sequence = (...patterns) => {
	const parser = parserFn(parserState => {
		let nextState = parserState;
		const result = [];

		for (let pattern of patterns) {
			const test = pattern(nextState);

			if (test.isError) {
				return updateParserIndex(
					updateParserError(
						parserState,
						test.errorMsg
					),
					test.index
				)
			}

			result.push(test.result);
			nextState = updateParserState(
				parserState,
				test.index
			);
		}

		return updateParserResult(
			updateParserIndex(
				parserState,
				nextState.index
			),
			result
		);
	})

	parser.pattern = patterns.map(p => p.pattern);

	return parser;
}

export const optional = pattern => {
	const parser = parserState => {
		const test = pattern(parserState);
		if (test.isError) return parserState;
		return test;
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export const choice = (...patterns) => {
	const parser = parserFn(parserState => {
		let longest;

		for (let pattern of patterns) {
			const result = pattern(parserState);
			if (!result.isError) return result;

			if (!longest || result.index > longest.index) {
				longest = result;
			}
		}

		const [last, ...rest] = parser.pattern.reverse();
		return updateParserError(
			parserState,
			`Expected one of ${rest.reverse().map(x => `'${x}'`).join(', ')} or '${last}'`
		)
	})

	parser.pattern = patterns.map(p => p.pattern);
	return parser;
}

export const many = (pattern, minimum = 0) => {
	const parser = parserFn(parserState => {
		let nextState = parserState;
		let result = [];

		while (true) {
			if (nextState.index >= nextState.targetString.length) {
				return updateParserError(
					nextState,
					'Unexpected EOF'
				)
			}

			const test = pattern(nextState);
			if (test.isError) break;

			result.push(test.result);
			nextState = updateParserIndex(nextState, test.index);
		}

		if (result.length < minimum) return updateParserError(
			nextState,
			`Expected ${minimum} instances of '${pattern.pattern}' and only got ${result.length}`
		)

		return updateParserResult(
			updateParserIndex(
				parserState,
				nextState.index,
			),
			result
		);
	})

	parser.pattern = pattern.pattern;

	return parser;
}

export const posLookahead = pattern => {
	const parser = parserFn(parserState => {
		const test = pattern(parserState);

		if (test.isError) {
			return updateParserError(
				parserState,
				test.errorMsg
			);
		}

		return parserState;
	})

	parser.pattern = pattern.pattern;
	return parser;
}

export const negLookahead = pattern => {
	const parser = parserFn(parserState => {
		const test = pattern(parserState);

		if (test.isError) {
			return parserState;
		}

		return updateParserError(
			parserState,
			`Unexpected pattern '${pattern.pattern}'`
		)
	})

	parser.pattern = pattern.pattern;
	return parser;
}

export const until = pattern => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		const {
			targetString,
			index,
		} = parserState;

		const totalRemainingChars = targetString.length - index;

		let result = '';
		let count = 0;
		let nextState = parserState;

		while (true) {
			if (count > totalRemainingChars) {
				return updateParserError(
					nextState,
					'Unexpected EOF'
				);
			}

			const test = pattern(nextState);
			if (!test.isError) break;

			const currChar = nextState.targetString[nextState.index];
			result = result + currChar;

			nextState = updateParserState(
				nextState,
				nextState.index + 1,
			);

			count++;
		}

		return updateParserResult(nextState, result);
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export const EOF = () => {
	const parser = parserState => {
		if (parserState.index > parserState.targetString.length) {
			return updateParserError(
				parserState,
				'Unexpected EOF'
			)
		}

		if (parserState.index === parserState.targetString.length) {
			return updateParserResult(
				updateParserIndex(
					parserState,
					parserState.index + 1
				),
				'EOF'
			)
		}

		return updateParserError(
			parserState,
			`Expected 'EOF' and got '${parserState.targetString[parserState.index]}' instead`
		)
	}

	parser.pattern = 'EOF';
	return parser;
}