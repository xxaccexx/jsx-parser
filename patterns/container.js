import { whitespace } from "./chars.js";
import { parserFn, updateParserError, updateParserIndex, updateParserResult } from "./state.js"
import { choice, sequence, optional, many, str, until, EOF } from "./syn.js"
import { closingTag, openingTag, selfClosingTag, TAG_TYPES } from "./tags.js";

export const content = () => {
	const pattern = until(str('<'));
	const parser = parserFn(parserState => pattern(parserState));
	parser.pattern = pattern;
	return parser;
}

export const elementTree = () => {
	const contentParser = until(str('<'));

	const untilEnd = sequence(
		optional(many(whitespace())),
		EOF()
	);

	const tagParser = choice(
		openingTag(),
		closingTag(),
		selfClosingTag()
	);

	const parser = parserFn(parserState => {
		//	enforce opening root element
		let nextState = sequence(
			optional(many(whitespace())),
			openingTag()
		)(parserState);

		if (nextState.isError) return nextState;
		let [, root] = nextState.result;

		nextState = updateParserResult(
			nextState,
			root
		);

		while (nextState.index < parserState.targetString.length) {
			//	assume anything until the next tag is text
			//	TODO: interperlate the {n} syntax within the content
			const contentState = contentParser(nextState);

			if (contentState.isError) return contentState;
			root.children = [ ...(root.children ?? []), contentState.result ];
			
			const tagState = tagParser(contentState);

			if (tagState.isError) return tagState;

			let { result: tag } = tagState;
			// new tags should always have a ref to their root
			tag = { ...tag, parent: root };


			//	self and open types are counted as children of root
			if ([TAG_TYPES.SELF, TAG_TYPES.OPEN].includes(tag.type)) {
				const { parent, ...record } = tag
				root.children = [ ...root.children, record ];
			}

			//	open tag type will become the new root
			if (tag.type === TAG_TYPES.OPEN) {
				root = tag;
			}

			//	close tag type will move up to root.parent
			if (tag.type === TAG_TYPES.CLOSE) {
				//	but only if the names match
				if (root.tag.name !== tag.tag.name) {
					return updateParserError(
						nextState,
						`Closing tag does not match opening tag. opening as: '${ root.tag.name }', but closed as '${ tag.tag.name }'`
					)
				}

				root = root.parent;
			}
			
			nextState = updateParserIndex(
				nextState,
				tagState.index
			);

			if (!root) {
				const test = untilEnd(nextState);

				nextState = updateParserIndex(
					nextState,
					test.index,
				);

				break;
			}
		}

		console.log('preEOF', nextState)

		const EOFState = sequence(until(EOF()), EOF())(nextState);
		console.log('postEOF', EOFState)
		if (EOFState.isError) return EOFState;


		return updateParserIndex(
			nextState,
			EOFState.index
		);
	});

	parser.pattern = 'Element Tree';
	return parser;
}