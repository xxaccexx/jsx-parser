// import { inspect } from 'util';
import { elementTree } from './container.js'


/*
const example = 'Error\n' +
'    at REPL1:1:1\n' +
'    at Script.runInThisContext (node:vm:133:18)\n' +
'    at REPLServer.defaultEval (node:repl:508:29)\n' +
'    at bound (node:domain:416:15)\n' +
'    at REPLServer.runBound [as eval] (node:domain:427:12)\n' +
'    at REPLServer.onLine (node:repl:834:10)\n' +
'    at REPLServer.emit (node:events:391:22)\n' +
'    at REPLServer.EventEmitter.emit (node:domain:470:12)\n' +
'    at REPLServer.Interface._onLine (node:readline:364:10)\n' +
'    at REPLServer.Interface._line (node:readline:700:8)';
*/

export const stackParser = () => {
	const eStack = new Error().stack;
	const [, ...stackLines] = eStack.split('\n');

	const stack = stackLines
		.map(record => record.trimStart())	//	no spaces
		.map(record => record.substr(3))		//	no 'at '
		.map(record => ({ record }))				//	make object

		//	split out methods
		.map(item => {
			if (item.record.includes('(')) {
				const end = item.record.indexOf('(') - 1;
				const file = item.record.indexOf('(') + 1;

				const method = item.record.substring(0, end);
				const record = item.record.substr(file).replace(')', '');

				return {
					...item,
					record,
					method
				}
			}

			return item;
		})

		//	split out column number
		.map(item => {
			const start = item.record.lastIndexOf(':');
			const col = item.record.substr(start + 1);
			const record = item.record.substring(0, start);

			return {
				...item,
				record,
				col: isNaN(col) ? col : parseFloat(col)
			}
		})

		//	split out line number
		.map(item => {
			const start = item.record.lastIndexOf(':');
			const line = item.record.substr(start + 1);
			const record = item.record.substring(0, start);

			return {
				...item,
				record,
				line: isNaN(line) ? line : parseFloat(line)
			}
		})

		//	remove internal scripts
		.filter(item => item.record.indexOf('node:') !== 0)

		//	remove protocol
		.map(item => ({
			...item,
			record: item.record.replace('file://', '')
		}))

	// //	remove other runtime contexts
	// .map(item => {
	// 	const 
	// })

	// console.log(inspect(stack, false, 10000, true))
	return stack;
}


export const run = (pattern, targetString) => {
	const initialState = {
		targetString,
		index: 0,
		result: undefined,
		isError: false,
		errorMsg: undefined
	};

	return pattern(initialState);
}


export const parse = (input) => {
	const finalState = run(elementTree(), input);
	const {
		isError,
		errorMsg,
		index,
	} = finalState;

	if (isError) {
		const stack = stackParser();

		console.error(`JSX Parser error: ${errorMsg}`);
		console.error(stack)

		// const pointer = [ ...new Array(exec.index + 1) ].join(' ');
	}

	console.log(finalState);
}