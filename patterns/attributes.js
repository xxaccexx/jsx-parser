import { parserFn, updateParserResult } from './state.js';
import { tagName } from './variables.js';
import {
	str,
	sequence,
	until,
	choice,
	optional,
	many
} from './syn.js';
import { whitespace } from './chars.js';


export const singleQuoteAttrValue = () => {
	const pattern = sequence(
		str("'"),
		until(str("'")),
		str("'")
	);

	const parser = parserFn(parserState => pattern(parserState));
	parser.pattern = pattern.pattern;
	return parser;
}

export const doubleQuoteAttrValue = () => {
	const pattern = sequence(
		str('"'),
		until(str('"')),
		str('"')
	);

	const parser = parserFn(parserState => pattern(parserState));
	parser.pattern = pattern.pattern;
	return parser;
}

export const interpretedAttrValue = () => {
	const pattern = sequence(
		str('{'),
		until(str('}')),
		str('}')
	);

	const parser = parserFn(parserState => pattern(parserState));
	parser.pattern = pattern;
	return parser;
}

export const attrValue = () => {
	const pattern = choice(
		interpretedAttrValue(),
		singleQuoteAttrValue(),
		doubleQuoteAttrValue()
	);

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;

		let [ open, value, close ] = test.result;
		const type = open === close ? 'string' : 'interpreted';

		if (type === 'interpreted' && !isNaN(value)) value = parseFloat(value);

		return updateParserResult(
			test,
			{
				type,
				value
			}
		);
	});

	parser.pattern = pattern.pattern;
	return parser;
}


export const attribute = () => {
	const pattern = sequence(
		tagName(),
		optional(
			sequence(
				optional(many(whitespace())),
				str('='),
				optional(many(whitespace())),
				attrValue()
			)
		)
	);

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;
		
		const [ left, assignment ] = test.result;
		const [ ,,, right ] = assignment ?? [];

		const type = !right ? 'boolean' : right.type;
		const { value: name } = left;
		const value = type == 'boolean' ?	true : right.value;

		return updateParserResult(
			test,
			{
				type,
				name,
				value
			}
		)
	});
	parser.pattern = pattern.pattern;
	return parser;
}