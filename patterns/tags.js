import { parserFn, updateParserResult } from './state.js';

import {
	str,
	sequence,
	many,
	optional,
} from './syn.js';

import { whitespace } from './chars.js';
import { objectRef } from './variables.js';
import { attribute } from './attributes.js';


export const TAG_TYPES = {
	OPEN: 'opening_tag',
	CLOSE: 'closing_tag',
	SELF: 'self_closing_tag'
}


export const openingTag = () => {
	const pattern = sequence(
		str('<'),
		optional(many(whitespace())),
		objectRef(),

		optional(
			many(
				sequence(
					many(whitespace()),
					attribute()
				)
			)
		),

		optional(many(whitespace())),
		str('>')
	);

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;

		let [,,	tag, attributes ,,] = test.result;

		attributes = attributes.map(([, attr ]) => attr);

		return updateParserResult(
			test,
			{
				type: TAG_TYPES.OPEN,
				tag,
				attributes,
			}
		);
	});

	parser.pattern = pattern.pattern;
	return parser;
}


export const closingTag = () => {
	const pattern = sequence(
		str('</'),
		optional(many(whitespace())),
		objectRef(),
		optional(many(whitespace())),
		str('>')
	)

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;

		const [,, tag ,,] = test.result;

		return updateParserResult(
			test,
			{
				tag,
				type: TAG_TYPES.CLOSE
			}
		);
	})

	parser.pattern = pattern.pattern;
	return parser;
}


export const selfClosingTag = () => {
	const pattern = sequence(
		str('<'),
		optional(many(whitespace())),
		objectRef(),

		optional(
			many(
				sequence(
					many(whitespace()),
					attribute()
				)
			)
		),

		optional(many(whitespace())),
		str('/>')
	);

	const parser = parserFn(parserState => {
		const test = pattern(parserState);
		if (test.isError) return test;

		let [,,	tag, attributes ,,] = test.result;

		attributes = attributes.map(([, attr]) => attr);

		return updateParserResult(
			test,
			{
				type: TAG_TYPES.SELF,
				tag,
				attributes,
			}
		);
	});

	parser.pattern = pattern.pattern;
	return parser;
}
