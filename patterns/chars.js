import { updateParserError } from '../patterns/state.js';

import {
	choice,
	str,
} from './syn.js';


export const charSet = chars => {
	const charSet = choice(
		...chars
				.split('')
				.map(char => str(char))
	);

	let [ last, ...start ] = chars.split('').reverse();
	start = start.reverse().map(x => `'${x}'`).join(', ');
	last = `'${ last }'`;

	return parserState => {
		if (parserState.isError) return parserState;

		const result = charSet(parserState);

		if (result.isError) {
			return updateParserError(
				result,
				`Could not match one of ${ start } or ${ last }`
			)
		}

		return result
	}
}


const whitespaceParser = charSet(' \n\t');
export const whitespace = () => whitespaceParser;

const numbersParser = charSet('0123456789');
export const numbers = () => numbersParser;

const lowercaseParser = charSet('abcdefghijklmnopqrstuvwxyz');
export const lowercase = () => lowercaseParser;

const uppercaseParser = charSet('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
export const uppercase = () => uppercaseParser;


const alphabetParser = choice(lowercase(), uppercase());
export const alphabet = () => parserState => {
	if (parserState.isError) return parserState;

	const result = alphabetParser(parserState);

	if (result.isError) {
		return updateParserError(
			result,
			'Character is not alphabetic'
		)
	}

	return result;
}

const alphaNumericParser = choice(lowercase(), uppercase(), numbers());
export const alphaNumeric = () => parserState => {
	if (parserState.isError) return parserState;

	const result = alphaNumericParser(parserState);

	if (result.isError) {
		return updateParserError(
			result,
			'Character is special, expected alpha-numeric'
		)
	}

	return result;
}
