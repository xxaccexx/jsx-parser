import { parse as html } from './parse';

test('Can parse a simple self closing tag', () => {
	expect(html`<p />`).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: []
	})

	expect(html`<p prop prop="asd" prop=${123} />`).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [
			{ $$type: 'property', $$name: 'prop', $$value: true },
			{ $$type: 'property', $$name: 'prop', $$value: 'asd' },
			{ $$type: 'property', $$name: 'prop', $$value: 123 }
		]
	})

	expect(
		html`
			<p
				prop
				prop="asd"
				prop=${123}
			/>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [
			{ $$type: 'property', $$name: 'prop', $$value: true },
			{ $$type: 'property', $$name: 'prop', $$value: 'asd' },
			{ $$type: 'property', $$name: 'prop', $$value: 123 }
		]
	})
})

test('can parse a open/close element', () => {
	expect(html`<p></p>`).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [],
		$$children: [
			{
				$$type: 'content',
				$$value: ''
			}
		]
	})

	expect(html`<p prop prop="asd" prop=${123}></p>`).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [
			{ $$type: 'property', $$name: 'prop', $$value: true },
			{ $$type: 'property', $$name: 'prop', $$value: 'asd' },
			{ $$type: 'property', $$name: 'prop', $$value: 123 }
		],
		$$children: [
			{
				$$type: 'content',
				$$value: ''
			}
		]
	})

	expect(
		html`
			<p
				prop
				prop="asd"
				prop=${123}
			>
			</p>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [
			{ $$type: 'property', $$name: 'prop', $$value: true },
			{ $$type: 'property', $$name: 'prop', $$value: 'asd' },
			{ $$type: 'property', $$name: 'prop', $$value: 123 }
		],
		$$children: [
			{
				$$type: 'content',
				$$value: '\n\t\t\t'
			}
		]
	})
})

test('can parse a open/close element with text content', () => {
	expect(html`<p>something</p>`).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [],
		$$children: [
			{
				$$type: 'content',
				$$value: 'something'
			}
		]
	})

	expect(
		html`
			<p>
				something
			</p>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [],
		$$children: [
			{
				$$type: 'content',
				$$value: '\n\t\t\t\tsomething\n\t\t\t'
			}
		]
	})
})

test('can parse open/close element with child self closing tag', () => {
	expect(
		html`
			<p>
				<span />
			</p>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [],
		$$children: [
			{
				$$type: 'content',
				$$value: '\n\t\t\t\t'
			},
			{
				$$type: 'element',
				$$tag: 'span',
				$$props: []
			},
			{
				$$type: 'content',
				$$value: '\n\t\t\t'
			},
		]
	})

	expect(
		html`
			<p>
				<span

				/>
			</p>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'p',
		$$props: [],
		$$children: [
			{
				$$type: 'content',
				$$value: '\n\t\t\t\t'
			},
			{
				$$type: 'element',
				$$tag: 'span',
				$$props: []
			},
			{
				$$type: 'content',
				$$value: '\n\t\t\t'
			},
		]
	})
})

test('can parse open/close element with event syntax', () => {
	const handler = () => console.log('test');

	expect(
		html`
			<button
				type="button"
				@click=${handler}
			>
				Press me
			</button>
		`
	).toEqual({
		$$type: 'element',
		$$tag: 'button',
		$$props: [
			{ $$type: 'property', $$name: 'type', $$value: 'button' },
			{ $$type: 'property', $$name: '@click', $$value: handler }
		],
		$$children: [
			{
				$$type: 'content',
				$$value: '\n\t\t\t\tPress me\n\t\t\t'
			}
		]
	})
})