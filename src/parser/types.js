export const TYPES = {
    ELEMENT: 'ELEMENT',
    COMPONENT: 'COMPONENT',
    EXPRESSION: 'EXPRESSION',
    ENTITY: 'ENTITY',
    CONTENT: 'CONTENT',
}

const typeKeys = Object.keys(TYPES);

let nodeCount = 0;

export const basicNode = (config) => {
    const {
        $$type,
        ref,
        props = {},
        ...others
    } = config;
        
    if (!$$type)
        throw new TypeError('Type is required');

    if (!typeKeys.includes($$type))
        throw new TypeError(`'${ $$type }' is not a supported type`);

    if (!ref || !(typeof ref === 'string' || typeof ref === 'number') )
        throw new TypeError('ref must be a string or a number');

    if (!props && typeof props !== 'object')
        throw new TypeError('props must be an object');
    
    const node = {
        ...others,
        ref,
        props,
        $$type,
        $$id: nodeCount,
    };

    nodeCount ++;
    return node;
}


const elementNode = (name, props, children, ref) => basicNode({
    $$type: TYPES.ELEMENT,
    name,
    props,
    children,
    ref,
})

const componentNode = (name, props, children, ref, state) => basicNode({
    $$type: TYPES.COMPONENT,
    name,
    props,
    children,
    ref,
    state,
})

const expressionNode = (index) => basicNode({
    $$type: TYPES.EXPRESSION,
    index,
})

const entityNode = (entity) => basicNode({
    $$type: TYPES.ENTITY,
    entity,
})

const contentNode = (content) => basicNode({
    $$type: TYPES.CONTENT,
    content,
})
