import {
	chain,
	map,
} from "./parse"

import {
	str,
	sequence,
	many,
	whitespace,
	alphabet,
	choice,
	numbers,
	optional,
	alphaNumeric,
	until,
} from "./patterns"

/**
 * 1 Element
 * 2 self closed tag
 * 3 open tag
 * 4 close tag
 * 5 content
 * 6 expression
 * 
 * 1 -> 2|3|4
 * 2 -> exit
 * 3 -> 5
 * 4 -> exit
 * 5 -> 4|6|1
 * 6 -> 5
 */


const tagName = () => map(
	sequence(
		alphabet(),
		optional(
			many(
				choice(
					alphabet(),
					numbers(),
					str('-')
				)
			)
		)
	),
	r => r.flat(Infinity).join('')
)

//	6
const expression = () => map(
	sequence(
		str('\${'),
		many(numbers(), 1),
		str('}'),
	),
	([_, numbers], refs) => {
		const $$ref = parseInt(numbers);
		return {
			$$type: 'expression',
			$$value: refs[$$ref],
			$$ref,
		}
	}
)

const attrName = () => map(
	sequence(
		optional(str('@')),
		alphabet(),
		optional(many(alphaNumeric()))
	),
	$$name => ({ $$type: 'property', $$name: $$name.flat(Infinity).join('') })
)

const boolAttr = () => map(
	attrName(),
	r => ({ ...r, $$value: true })
)

const strAttr = () => map(
	sequence(
		attrName(),
		str('="'),
		until(str('"')),
		str('"')
	),
	([r, _, $$value]) => ({ ...r, $$value })
)

const exprAttr = () => map(
	sequence(
		attrName(),
		str('='),
		expression(),
	),
	([r, _, $$expr]) => ({ ...r, $$value: $$expr.$$value })
)

const openTagNameWithAttrs = () => map(
	sequence(
		str('<'),
		tagName(),
		optional(
			many(
				map(
					sequence(
						many(whitespace(), 1),
						choice(
							strAttr(),
							exprAttr(),
							boolAttr(),
						)
					),
					([_, r]) => r
				)
			)
		)
	),
	([_, $$tag, $$props]) => ({
		$$type: 'unknown-tag',
		$$tag,
		$$props,
	})
)

//	3
const openingTag = () => (
	map(
		chain(
			map(
				sequence(
					openTagNameWithAttrs(),
					optional(many(whitespace(), 1)),
					str('>')
				),
				([r]) => ({ ...r, $$type: 'open-tag' })
			),
			r => content(r.$$tag)	//> 5
		),
		([openTag, children]) => {
			return {
				...openTag,
				$$children: [
					...children
				]
			}
		}
	)
)

//	2
const selfClosingTag = () => map(
	sequence(
		openTagNameWithAttrs(),
		many(whitespace(), 1),
		str('/>')
	),
	([r]) => {
		const tag = ({ ...r, $$type: 'self-closing-tag' });
		return tag
	}
)

//	4
const closingTag = (tagName) => map(
	sequence(
		str('</'),
		str(tagName),
		str('>'),
	),
	([_, $$tag]) => ({ $$type: 'closing-tag', $$tag })
)


//	5
const content = (tagName) => (
	choice(
		//> 6
		map(
			chain(
				until(str('${')),
				() => chain(
					expression(), //> 6 > 5
					() => content(tagName) //> 5
				)
			),
			([$$value, ...others]) => [
				{
					$$type: 'content',
					$$value,
				},
				...others.flat(Infinity)
			]
		),

		//> 1
		map(
			chain(
				until(str('<')),
				() => chain(
					element(),
					() => content(tagName)
				)
			),
			([$$value, ...others]) => [
				{
					$$type: 'content',
					$$value,
				},
				...others.flat(Infinity)
			]
		),

		//> 4
		map(
			chain(
				until(str('</')),
				() => closingTag(tagName)	//> 4
			),
			([$$value]) => [
				{
					$$type: 'content',
					$$value,
				}
			]
		),
	)
)

//	1
const element = () => map(
	choice(
		selfClosingTag(),	//> 2
		openingTag(),	//> 3
	),
	r => ({
		...r,
		$$type: 'element'
	})
)


//	AST
const ast = () => map(
	sequence(
		optional(many(whitespace(), 1)),
		element(),
		optional(many(whitespace(), 1)),
	),
	([, element]) => element
)

export default ast