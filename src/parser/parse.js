import { inspect } from 'util'
import ast from './ast'
import { updateParserError, updateParserResult } from './state'

//	execute pattern matching on a string
export const run = (pattern, targetString, references = []) => {
	const initialState = {
		targetString,
		references,
		index: 0,
		result: undefined,
		isError: false,
		errorMsg: undefined,
	};

	return pattern(initialState);
}

//	no change, a no-op parser
export const noOp = () => parserState => parserState;

//	replace the results of the pattern with another value
export const map = (pattern, mapFn) => parserState => {
	const nextState = pattern(parserState);
	if (nextState.isError) return nextState;

	if (!mapFn) return parserState;
	const mappedResult = mapFn(nextState.result, nextState.references);
	return updateParserResult(nextState, mappedResult);
}

//	choose the next pattern based the the result of the last pattern
export const chain = (pattern, mapFn) => parserState => {
	const nextState = pattern(parserState);
	if (nextState.isError) return nextState;

	let nextPattern = mapFn(nextState.result);
	if (!nextPattern) return nextState;

	const finalState = nextPattern(nextState);
	return updateParserResult(
		finalState,
		[nextState.result, finalState.result]
	)
}

//	injects a defined error
export const failed = errorFn => parserState => {
	const { index, targetString } = parserState
	const msg = errorFn(targetString[index], index, targetString)
	return updateParserError(
		parserState,
		msg
	)
}

//	run a pattern and inject a specific result
export const withResult = (result, pattern) => parserState => pattern(
	updateParserResult(
		parserState,
		result
	)
)

const errorPointer = (pos = 0) => {
	const len = [
		...new Array(
			Math.max(pos, 0) + 1
		)
	].map(_ => '-');
	return `${len.join('')}^`;
}

export const printError = (input = '', _index) => {
	let index = _index;

	const lines = input.split('\n');

	let remaining = index;
	const details = (
		lines
			.map((line, index) => [index, line.length])
			.map(([line, len]) => {
				const r = remaining;
				remaining -= len;
				return [line, len, r, remaining <= 0];
			})
	)

	const errDetail = details.find(([, , , errorLine]) => errorLine) ?? [];
	const [errLine, , errPos] = errDetail;

	const newLines = (
		lines
			.map((l, i) => `${((i + 1) + '').padEnd(4, ' ')}| ${l}`)
			.reduce((str, content, index, arr) => {
				let instances = 0;
				let line = content;

				while (line.indexOf('\t') > -1) {
					line = line.replace('\t', '  ');
					instances++;
				}

				const newStr = [...str, line];


				if (index === errLine) {
					const pos = instances + errPos;
					const pointer = errorPointer(pos);
					return [...newStr, `    |${pointer}`];
				}

				return newStr;
			}, [])
	)

	return newLines.join('\n')
}

export const parse = (segments = [], ...values) => {
	const template = segments.reduce((str, seg, index) => {
		if (index <= values.length - 1) return `${str}${seg}\$\{${index}\}`
		return `${str}${seg}`;
	}, '');

	const result = run(ast(), template, values)

	let printStr = [];

	if (result.isError) {
		printStr.push('---------------------------------------------------------------------------');
		printStr.push('Error:');
		printStr.push(result.errorMsg);
		printStr.push('---------------------------------------------------------------------------');
		printStr.push(printError(result.targetString, result.index));
		printStr.push('---------------------------------------------------------------------------');

		console.log(['', ...printStr, ''].join('\n'))
		if (result.isError) console.error(result.errorMsg);
	}
	// else {
	// 	printStr.push('---------------------------------------------------------------------------');
	// 	printStr.push('Success!');
	// 	if (result.index < result.targetString.length)
	// 		printStr.push('But the document wasn\'t completely consumed...');
	// 	printStr.push('---------------------------------------------------------------------------');
	// 	printStr.push(printError(result.targetString, result.index));
	// 	printStr.push('---------------------------------------------------------------------------');
	// 	printStr.push(inspect(result.result, false, 20, true));
	// 	printStr.push('---------------------------------------------------------------------------');
	// }

	return result.result
}