import { printError } from "./parse"

const example = `something
to test will 
		be written
here. This will allow me
	to test
some content easily.
aha ya`;

test('print a thingy', () => {
	expect(
		printError(
			example,
			64
		)
	)
	.toEqual(`1   | something
2   | to test will 
3   |     be written
4   | here. This will allow me
5   |   to test
    |--------^
6   | some content easily.
7   | aha ya`
	)

	expect(
		printError(
			example,
			32
		)
	)
	.toEqual(`1   | something
2   | to test will 
3   |     be written
    |-------------^
4   | here. This will allow me
5   |   to test
6   | some content easily.
7   | aha ya`
	)
	
})