import {
	sequence,
	str,
	tagName,
} from '..'
import { map } from '../parse'

const closeTag = () => map(
	sequence(
		str('</'),
		tagName(),
		str('>')
	),
	([_, $$tag]) => ({ $$type: 'close-tag', $$tag })
)

export default closeTag