import {
	sequence,
	str,
	optional,
	many,
	whitespace,
	attribute,
	tagName,
} from '..'
import { map } from '../parse';

const selfClosedTag = () => map(
	sequence(
		str('<'),
		tagName(),
		optional(
			many(
				map(
					sequence(
						many(whitespace()),
						attribute(),
					),
					([_, r]) => r
				)
			)
		),
		many(whitespace()),
		str('/>'),
	),
	([_, $$tag, $$props ]) => ({
		$$type: 'self-closed-tag',
		$$tag,
		$$props,
	})
)

export default selfClosedTag