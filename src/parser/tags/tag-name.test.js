import { tagName } from '.'
import { defineTest } from '../../../test-helper'

test('matches tag name', () => {
	defineTest(
		tagName(),
		'something',
		9,
		{
			$$type: 'tag-name',
			$$name: 'something'
		}
	)
})

test('matches tag name with hyphen', () => {
	defineTest(
		tagName(),
		'some-thing',
		10,
		{
			$$type: 'tag-name',
			$$name: 'some-thing'
		}
	)
})

test('matches tag name with numbers', () => {
	defineTest(
		tagName(),
		'som3-th1ng',
		10,
		{
			$$type: 'tag-name',
			$$name: 'som3-th1ng'
		}
	)
})

test('these should not work', () => {
	defineTest(
		tagName(),
		'5omething',
		0,
		undefined,
		'Character is not alphabetic'
	)

	defineTest(
		tagName(),
		'-something',
		0,
		undefined,
		'Character is not alphabetic'
	)

	defineTest(
		tagName(),
		'$omething',
		0,
		undefined,
		'Character is not alphabetic'
	)
})