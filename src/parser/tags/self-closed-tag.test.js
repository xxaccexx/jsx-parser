import { selfClosedTag } from '.'
import { defineTest } from '../../../test-helper'

test('matches simple self closing tag', () => {
	defineTest(
		selfClosedTag(),
		'<tag />',
		7,
		{
			$$type: 'self-closed-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'tag'
			},
			$$props: []
		}
	)
})

test('matches simple self closing tag with props', () => {
	defineTest(
		selfClosedTag(),
		'<tag prop prop="value" prop=${123} />',
		37,
		{
			$$type: 'self-closed-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'tag'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: true
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'content',
						$$value: 'value'
					}
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'expression',
						$$ref: 123
					}
				},
			]
		}
	)
})

test('matches simple self closing tag with props and heavy spacing', () => {
	defineTest(
		selfClosedTag(),
		`<tag
			prop
			prop="value"
			prop=\${123}
		/>`,
		48,
		{
			$$type: 'self-closed-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'tag'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: true
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'content',
						$$value: 'value'
					}
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'expression',
						$$ref: 123
					}
				},
			]
		}
	)
})