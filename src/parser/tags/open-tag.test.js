import { openTag } from '.'
import { defineTest } from '../../../test-helper'

test('matches an opening tag', () => {
	defineTest(
		openTag(),
		'<test>',
		6,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: []
		}
	)
})

test('matches an opening tag with a boolean prop', () => {
	defineTest(
		openTag(),
		'<test prop>',
		11,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: true
				}
			]
		}
	)
})

test('matches an opening tag with a string prop', () => {
	defineTest(
		openTag(),
		'<test prop="value">',
		19,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'content',
						$$value: 'value'
					}
				}
			]
		}
	)
})

test('matches an opening tag with a property-ref', () => {
	defineTest(
		openTag(),
		'<test prop=${123}>',
		18,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'expression',
						$$ref: 123
					}
				}
			]
		}
	)
})

test('matches an opening tag with all three', () => {
	defineTest(
		openTag(),
		'<test prop prop="value" prop=${123}>',
		36,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: true,
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'content',
						$$value: 'value'
					}
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'expression',
						$$ref: 123
					}
				}
			]
		}
	)
})

test('same again, but with whitespace', () => {
	defineTest(
		openTag(),
		`<test
			prop
			prop="value"
			prop=\${123}
		>`,
		48,
		{
			$$type: 'open-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'test'
			},
			$$props: [
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: true,
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'content',
						$$value: 'value'
					}
				},
				{
					$$type: 'property',
					$$prop: 'prop',
					$$value: {
						$$type: 'expression',
						$$ref: 123
					}
				}
			]
		}
	)
})