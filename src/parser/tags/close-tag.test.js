import { closeTag } from '.'
import { defineTest } from '../../../test-helper'

test('matches a known tag name', () => {
	defineTest(
		closeTag(),
		'</tag>',
		6,
		{
			$$type: 'close-tag',
			$$tag: {
				$$type: 'tag-name',
				$$name: 'tag'
			}
		}
	)
})

test('No whitespace allowed', () => {
	defineTest(
		closeTag(),
		'</ tag>',
		2,
		undefined,
		"Character is not alphabetic"
	)
})