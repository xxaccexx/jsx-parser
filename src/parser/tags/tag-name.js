import {
	sequence,
	optional,
	many,
	choice,
	alphabet,
	numbers,
	str,
} from '..'
import { map } from '../parse'

const tagName = () => map(
	sequence(
		alphabet(),
		optional(
			many(
				choice(
					alphabet(),
					numbers(),
					str('-')
				)
			)
		)
	),
	r => ({
		$$type: 'tag-name',
		$$name: r.flat(Infinity).join('')
	})
)

export default tagName