import {
	sequence,
	str,
	optional,
	many,
	whitespace,
	attribute,
} from '..'
import { map } from '../parse';
import tagName from './tag-name';

const openTag = () => map(
	sequence(
		str('<'),
		tagName(),
		optional(
			many(
				map(
					sequence(
						many(whitespace(), 1),
						attribute(),
					),
					([_, r]) => r
				)
			)
		),
		optional(many(whitespace())),
		str('>')
	),
	([_, $$tag, $$props]) => ({
		$$type: 'open-tag',
		$$tag,
		$$props,
	})
)

export default openTag