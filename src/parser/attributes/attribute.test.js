import { attribute } from '.'
import { defineTest } from '../../../test-helper'

test('match full attribute with string value', () => {
	defineTest(
		attribute(),
		'prop="value"',
		12,
		{
			$$type: 'property',
			$$prop: 'prop',
			$$value: {
				$$type: 'content',
				$$value: 'value'
			}
		}
	)
})

test('match full attribute with expression', () => {
	defineTest(
		attribute(),
		'prop=${123}',
		11,
		{
			$$type: 'property',
			$$prop: 'prop',
			$$value: {
				$$type: 'expression',
				$$ref: 123
			}
		}
	)
})

test('match boolean attribute', () => {
	defineTest(
		attribute(),
		'prop ',
		4,
		{
			$$type: 'property',
			$$prop: 'prop',
			$$value: true
		}
	)
})

test('match boolean attribute at end of open tag', () => {
	defineTest(
		attribute(),
		'prop>',
		4,
		{
			$$type: 'property',
			$$prop: 'prop',
			$$value: true
		}
	)
})

test('match boolean attribute at end of self closing tag', () => {
	defineTest(
		attribute(),
		'prop/>',
		4,
		{
			$$type: 'property',
			$$prop: 'prop',
			$$value: true
		}
	)
})

