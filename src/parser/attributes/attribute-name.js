import {
	sequence,
	optional,
	str,
	many,
	choice,
	alphabet,
	numbers,
	charSet,
} from '..'
import { map } from '../parse';

const attributeName = () => map(
	sequence(
		choice(
			alphabet(),
			str('@'),
		),
		optional(
			many(
				choice(
					alphabet(),
					numbers(),
					charSet('-$@'),
				)
			)
		)
	),
	r => ({
		$$type: 'property',
		$$prop: r.flat(Infinity).join(''),
		$$value: true,
	})
)

export default attributeName