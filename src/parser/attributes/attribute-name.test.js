import { attributeName } from '.'
import { defineTest } from '../../../test-helper'

test('matches a attribute name', () => {
	defineTest(
		attributeName(),
		'asd    ',
		3,
		{
			$$type: 'property',
			$$prop: "asd",
			$$value: true,
		}
	)

	defineTest(
		attributeName(),
		'asd=',
		3,
		{
			$$type: 'property',
			$$prop: 'asd',
			$$value: true,
		}
	)

	defineTest(
		attributeName(),
		'asd>',
		3,
		{
			$$type: 'property',
			$$prop: 'asd',
			$$value: true,
		}
	)

	defineTest(
		attributeName(),
		'asd/>',
		3,
		{
			$$type: 'property',
			$$prop: 'asd',
			$$value: true,
		}
	)
})

test('this should be fine', () => {
	defineTest(
		attributeName(),
		'asd',
		3,
		{
			$$type: 'property',
			$$prop: 'asd',
			$$value: true,
		}
	)

	defineTest(
		attributeName(),
		'@asd',
		4,
		{
			$$type: 'property',
			$$prop: '@asd',
			$$value: true,
		}
	)

	defineTest(
		attributeName(),
		'asd-asd',
		7,
		{
			$$type: 'property',
			$$prop: 'asd-asd',
			$$value: true,
		}
	)
})

test('fails because starting is not alphabetic', () => {
	defineTest(
		attributeName(),
		'"asd',
		0,
		undefined,
		"Unexpected character '\"' @ 0"
	)

	defineTest(
		attributeName(),
		'2asd',
		0,
		undefined,
		"Unexpected character '2' @ 0"
	)

	defineTest(
		attributeName(),
		'-asd',
		0,
		undefined,
		"Unexpected character '-' @ 0"
	)
})
