import {
	sequence,
	str,
	until,
} from '..'
import { map } from '../parse';
import { updateParserResult } from '../state';

const attributeValue = () => map(
	sequence(
		str('"'),
		until(str('"')),
		str('"'),
	),
	r => ({
		$$type: 'content',
		$$value: r[1]
	})
)

export default attributeValue