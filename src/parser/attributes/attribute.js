import {
	sequence,
	optional,
	str,
	choice,
	expression,
} from '..';

import {
	attributeName,
	attributeValue,
} from '.'
import { updateParserError, updateParserResult } from '../state';
import { map } from '../parse';

const attribute_ = () => {
	const pattern = sequence(
		attributeName(),
		optional(
			sequence(
				str('='),
				choice(
					expression(),
					attributeValue()
				)
			)
		)
	)

	const parser = parserState => {
		if (parserState.isError) return parserState;

		const nextState = pattern(parserState);
		if (nextState.isError) return nextState;

		const [left, right] = nextState.result;
		const { $$value: $$prop } = left;

		if (!right) {
			return updateParserResult(
				nextState,
				{
					$$type: 'property',
					$$value: true,
					$$prop,
				}
			)
		}

		const [, value] = right;

		switch (value.$$type) {
			case 'content': {
				return updateParserResult(
					nextState,
					{
						$$type: 'property',
						$$value: value.$$value,
						$$prop,
					}
				)
			}

			case 'expression': {
				return updateParserResult(
					nextState,
					{
						$$type: 'property-ref',
						$$ref: value.$$ref,
						$$prop,
					}
				)
			}

			default: {
				return updateParserError(
					nextState,
					'Attribute value is malformed'
				)
			}
		}
	}

	parser.pattern = pattern.pattern;
	return parser;
}

const attribute = () => map(
	sequence(
		attributeName(),
		map(
			optional(
				map(
					sequence(
						str('='),
						choice(
							expression(),
							attributeValue()
						)
					),
					([_, r]) => r
				)
			),
			r => {
				if (!r) return true;
				return r;
			}
		)
	),
	([r, $$value]) => Object.assign({}, r, { $$value })
)

export default attribute