import { attributeValue } from '.'
import { defineTest } from '../../../test-helper'

test('matches an attribute value', () => {
	defineTest(
		attributeValue(),
		'"asd"',
		5,
		{
			$$type: 'content',
			$$value: "asd"
		}
	)
})

test('attributes require start quotes', () => {
	defineTest(
		attributeValue(),
		'asd"',
		0,
		undefined,
		"Expected '\"' and got 'a' instead"
	)
})

test('attributes require end quotes', () => {
	defineTest(
		attributeValue(),
		'"asd',
		5,
		undefined,
		"Unexpected EOF"
	)
})
