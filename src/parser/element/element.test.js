import { element } from '.'
import { defineTest } from '../../../test-helper'

test('can parse a simple document', () => {
	defineTest(
		element(),
		`
		<p>
			something
		</p>
		`,
		26,
		{
			$$type: 'element',
			$$name: 'p',
			$$props: [],
			$$children: [
				{
					$$type: 'content',
					$$value: '\n\t\t\tsomething\n\t\t',
				}
			]
		}
	)
})

test('fails without matching element', () => {
	defineTest(
		element(),
		`
		<p>
			something
		</a>
		`,
		26,
		undefined,
		'missing matching closing tag'
	)
})