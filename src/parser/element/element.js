import {
	chain,
	failed,
	map,
	noOp,
} from '../parse';

import {
	until,
	charSet,
	sequence,
	choice,
	alphabet,
	optional,
} from '../patterns'

import {
	openTag,
	closeTag,
	selfClosedTag,
} from '../tags'

/**
 * 1 Element
 * 2 self closed tag
 * 3 open tag
 * 4 close tag
 * 5 content
 * 6 close tag
 * 7 expression
 * 
 * 1 -> 2|3|4
 * 2 -> exit
 * 3 -> 5
 * 4 -> error -> exit
 * 5 -> 6|7
 * 6 -> exit
 * 7 -> 5
 */

const tagName = () => map(
	sequence(
		alphabet(),
		optional(
			many(
				choice(
					alphabet(),

				)
			)
		)
	)
)

//	1 - Element - 2|3|4
const element = () => chain(
	choice(
		selfClosedTag(),
		openTag(),
	),
	r => {
		//	3 => 5
		if (r.$$type === 'open-tag') return content();

		return noOp();
	}
)

//	5 - content - 
const content = () => sequence(
	map(
		until(charSet('$<')),
		$$value => ({ $$type: 'content', $$value })
	),
	chain(
		choice(
			map(str('$'), r => 'expression'),
			map(str('<'), r => '')
		),

		type => {

		}
	)
)



export default element
