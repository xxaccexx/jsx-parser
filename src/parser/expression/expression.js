import { map } from '../parse';
import { sequence, str, many, numbers } from '../patterns';

const expression = () => map(
	sequence(
		str('$\{'),
		many(numbers(), 1),
		str('}')
	),
	r => {
		const [, digits] = r;
		return {
			$$type: 'expression',
			$$ref: parseInt(digits.join(''))
		}
	}
)


export default expression