import { defineTest } from '../../../test-helper';
import { expression } from '.';

test('expressions match numeric refs', () => {
	defineTest(
		expression(),
		'${123}',
		6,
		{
			$$type: 'expression',
			$$ref: 123,
		}
	)
})

test('expressions fail if NaN', () => {
	defineTest(
		expression(),
		'${hello}',
		2,
		undefined,
		"Expected 1 instances of '0,1,2,3,4,5,6,7,8,9' and only got 0"
	)
})

test('expressions fail if not wrapped correctly', () => {
	defineTest(
		expression(),
		'{hello}',
		0,
		undefined,
		"Expected '${' and got '{h' instead"
	)
})