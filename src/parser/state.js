export const updateParserState = (state, index, result, isError) => ({
	...state,
	index,
	result,
	isError: isError ?? state.isError		//	preserve previous isError, if it is set
});

export const updateParserIndex = (state, index) => ({
	...state,
	index
});

export const updateParserResult = (state, result) => ({
	...state,
	result
});

export const updateParserError = (state, errorMsg) => {
	return {
		...state,
		isError: true,
		errorMsg: new Error(errorMsg)
	}
};

export const removeParserError = (state) => {
	const { isError, errorMsg, ...rest } = state;
	return { ...rest, isError: false, errorMsg: undefined }
}
