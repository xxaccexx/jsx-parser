export const currentStack = () => {
	const eStack = new Error().stack;
	const [, ...stackLines] = eStack.split('\n');

	const stack = stackLines
		.map(record => record.trimStart())	//	no spaces
		.map(record => record.substr(3))		//	no 'at '
		.map(record => ({ record }))				//	make object

		//	split out methods
		.map(item => {
			if (item.record.includes('(')) {
				const end = item.record.indexOf('(') - 1;
				const file = item.record.indexOf('(') + 1;

				const method = item.record.substring(0, end);
				const record = item.record.substr(file).replace(')', '');

				return {
					...item,
					record,
					method
				}
			}

			return item;
		})

		//	split out column number
		.map(item => {
			const start = item.record.lastIndexOf(':');
			const col = item.record.substr(start + 1);
			const record = item.record.substring(0, start);

			return {
				...item,
				record,
				col: isNaN(col) ? col : parseFloat(col)
			}
		})

		//	split out line number
		.map(item => {
			const start = item.record.lastIndexOf(':');
			const line = item.record.substr(start + 1);
			const record = item.record.substring(0, start);

			return {
				...item,
				record,
				line: isNaN(line) ? line : parseFloat(line)
			}
		})

		//	remove internal scripts
		.filter(item => item.record.indexOf('node:') !== 0)

		//	remove protocol
		.map(item => ({
			...item,
			record: item.record.replace('file://', '')
		}))

	// //	remove other runtime contexts
	// .map(item => {
	// 	const 
	// })

	// console.log(inspect(stack, false, 10000, true))
	return stack;
}