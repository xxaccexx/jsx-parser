import { str } from '.';
import { defineTest } from '../../../test-helper';

test('matches simple strings', () => {
	defineTest(
		str('abc'),
		'abcdefg',
		3,
		'abc',
	)
});

test('matches simple strings half way through', () => {
	expect(
		str('abc')({
			targetString: '!abcdefg',
			index: 1,
			isError: false
		})
	).toEqual({
		targetString: '!abcdefg',
		index: 4,
		isError: false,
		result: 'abc'
	})
});

test('fails when the string doesnt match', () => {
	defineTest(
		str('abc'),
		'ab!defg',
		0,
		undefined,
		"Expected 'abc' and got 'ab!' instead"
	)
})

test('matches simple strings half way through', () => {
	expect(
		str('abc')({
			targetString: 'abc',
			index: 3,
			isError: false
		})
	).toEqual({
		targetString: 'abc',
		index: 3,
		isError: true,
		result: undefined,
		errorMsg: 'Unexpected EOF'
	})
});