import {
	updateParserError,
	updateParserState,
} from '../state';

const str = (pattern) => {
	if (typeof pattern !== 'string')
		throw new TypeError(`pattern must be a string`);

	const parser = parserState => {
		if (parserState.isError) return parserState;

		if (require('../../../package.json').debug)
			console.trace(`\nExpecting '${pattern}' in '${parserState.targetString.substr(parserState.index)}'`)

		if (parserState.index >= parserState.targetString.length) {
			return updateParserError(
				parserState,
				'Unexpected EOF'
			)
		}

		const test = parserState.targetString
			.substr(
				parserState.index,
				pattern.length
			);

		if (test === pattern) {
			return updateParserState(
				parserState,
				parserState.index + pattern.length,
				test,
			)
		}

		return updateParserError(
			parserState,
			`Expected '${pattern}' and got '${test}' instead`,
		)
	}

	parser.pattern = pattern;
	return parser;
}

export default str;