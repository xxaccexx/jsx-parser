import { updateParserError } from '../../state';
import { choice } from '../.';
import { lowercase, uppercase, numbers } from '.';

const alphaNumeric = () => {
	const pattern = choice(lowercase(), uppercase(), numbers());

	const parser = parserState => {
		if (parserState.isError) return parserState;

		const result = pattern(parserState);

		if (result.isError) {
			return updateParserError(
				result,
				'Character is special, expected alpha-numeric'
			)
		}

		return result;
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default alphaNumeric;