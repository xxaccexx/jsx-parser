import { updateParserError } from '../../state';
import { choice } from '../.';
import { lowercase, uppercase } from '.';


const alphabet = () => {
	const pattern = choice(lowercase(), uppercase());

	const parser = parserState => {
		if (parserState.isError) return parserState;

		const result = pattern(parserState);

		if (result.isError) {
			return updateParserError(
				result,
				'Character is not alphabetic'
			)
		}

		return result;
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default alphabet