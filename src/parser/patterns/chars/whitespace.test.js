import { whitespace } from '.';
import { defineTest } from '../../../../test-helper';

test('matches a space char', () => {
	defineTest(
		whitespace(),
		' ',
		1,
		' ',
	)
})

test('matches a tab char', () => {
	defineTest(
		whitespace(),
		'\t',
		1,
		'\t'
	)
})

test('matches a new line char', () => {
	defineTest(
		whitespace(),
		'\n',
		1,
		'\n',
	)
})

test('doesnt match any other char', () => {
	defineTest(
		whitespace(),
		'!',
		0,
		undefined,
		"Could not match one of ' ', '\n' or '\t'"
	)
})