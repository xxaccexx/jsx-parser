import { charSet } from '.'

const uppercase = () => {
	const pattern = charSet('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
	const parser = parserState => pattern(parserState);

	parser.pattern = pattern.pattern;
	return parser;
}

export default uppercase;