import { charSet } from '.'

const whitespace = () => {
	const pattern = charSet(' \n\t');
	const parser = parserState => pattern(parserState);
	parser.pattern = pattern.pattern;
	return parser;
}

export default whitespace