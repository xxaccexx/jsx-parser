import { uppercase, lowercase, alphabet } from '.';
import { defineTest } from '../../../../test-helper';

let charsUpper = '';
for (let i = 65; i < 91; i++) {
	charsUpper = charsUpper + String.fromCharCode(i);
}

let charsLower = '';
for (let i = 97; i < 123; i++) {
	charsLower = charsLower + String.fromCharCode(i);
}

const errorCreator = (chars) => {
	let [last, ...start] = (chars ?? '').split('').reverse();
	start = start.reverse().map(x => `'${x}'`).join(', ');
	last = `'${last}'`;

	return `Could not match one of ${start} or ${last}`
}

test('uppercase matches an uppercase letter', () => {
	defineTest(
		uppercase(),
		'C',
		1,
		'C',
	)
})

test('uppercase doesnt match a lowercase letter', () => {
	defineTest(
		uppercase(),
		'c',
		0,
		undefined,
		errorCreator(charsUpper)
	)
})

test('lowercase matches a lowercase letter', () => {
	defineTest(
		lowercase(),
		'c',
		1,
		'c'
	)
})

test('lowercase doesnt match an uppercase letter', () => {
	defineTest(
		lowercase(),
		'C',
		0,
		undefined,
		errorCreator(charsLower)
	)
})

test('alphabet matches any case', () => {
	defineTest(
		alphabet(),
		'C',
		1,
		'C',
	)

	defineTest(
		alphabet(),
		'c',
		1,
		'c',
	)
})

test('alphabet doesnt match numbers or special chars', () => {
	defineTest(
		alphabet(),
		'9',
		0,
		undefined,
		'Character is not alphabetic'
	)

	defineTest(
		alphabet(),
		'!',
		0,
		undefined,
		'Character is not alphabetic'
	)
})
