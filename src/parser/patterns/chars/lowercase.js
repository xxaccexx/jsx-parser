import { charSet } from '.'

const lowercase = () => {
	const pattern = charSet('abcdefghijklmnopqrstuvwxyz');
	const parser = parserState => pattern(parserState);

	parser.pattern = pattern.pattern;
	return parser;
}

export default lowercase