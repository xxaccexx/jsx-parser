import { alphaNumeric } from '.';
import { defineTest } from '../../../../test-helper';


test('alphaNumeric matches any number or letter of any case', () => {
	defineTest(
		alphaNumeric(),
		'a',
		1,
		'a',
	)

	defineTest(
		alphaNumeric(),
		'A',
		1,
		'A',
	)

	defineTest(
		alphaNumeric(),
		'9',
		1,
		'9',
	)
})

test('alphaNumeric doesnt match special chars', () => {
	defineTest(
		alphaNumeric(),
		'!',
		0,
		undefined,
		'Character is special, expected alpha-numeric'
	)
})
