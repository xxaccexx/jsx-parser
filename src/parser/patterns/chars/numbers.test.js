import { numbers } from '.';
import { defineTest } from '../../../../test-helper';

test('matches a numeric char', () => {
	defineTest(
		numbers(),
		'1',
		1,
		'1',
	)
})

test('matches a numeric char', () => {
	defineTest(
		numbers(),
		'9',
		1,
		'9',
	)
})

test('matches a numeric char', () => {
	defineTest(
		numbers(),
		' ',
		0,
		undefined,
		"Could not match one of '0', '1', '2', '3', '4', '5', '6', '7', '8' or '9'"
	)
})

test('matches a numeric char', () => {
	defineTest(
		numbers(),
		'a',
		0,
		undefined,
		"Could not match one of '0', '1', '2', '3', '4', '5', '6', '7', '8' or '9'"
	)
})