import { charSet } from '.';
import { defineTest } from '../../../../test-helper';

test('matches one of the chars in a string', () => {
	defineTest(
		charSet('abc'),
		'c',
		1,
		'c'
	)
})

test('doesnt match one of the chars in a string', () => {
	defineTest(
		charSet('abc'),
		'!',
		0,
		undefined,
		"Could not match one of 'a', 'b' or 'c'"
	)
})