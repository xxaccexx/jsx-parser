import { charSet } from '.'

const numbers = () => {
	const pattern = charSet('0123456789');
	const parser = parserState => pattern(parserState);
	parser.pattern = pattern.pattern;
	return parser;
}

export default numbers