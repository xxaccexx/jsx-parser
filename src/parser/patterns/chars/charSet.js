import { updateParserError } from '../../state';
import { choice, str } from '../.'

const charSet = chars => {
	const pattern = choice(
		...chars
			.split('')
			.map(char => str(char))
	);

	let [last, ...start] = chars.split('').reverse();
	start = start.reverse().map(x => `'${x}'`).join(', ');
	last = `'${last}'`;

	const parser = parserState => {
		if (parserState.isError) return parserState;

		const result = pattern(parserState);

		if (result.isError) {
			return updateParserError(
				result,
				`Could not match one of ${start} or ${last}`
			)
		}

		return result
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default charSet