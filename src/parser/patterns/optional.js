const optional = pattern => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		if (require('../../../package.json').debug)
			console.log('>> Enter Optional for', JSON.stringify(pattern.pattern))

		const test = pattern(parserState);

		if (require('../../../package.json').debug)
			console.log('>> Exit Optional for', JSON.stringify(pattern.pattern))

		if (test.isError) return parserState;
		return test;
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default optional;