import {
	updateParserState,
	updateParserResult,
	updateParserError,
} from '../state';

const until = (pattern, minLength = 0) => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		if (require('../../../package.json').debug)
			console.log('>> Enter until', JSON.stringify(pattern.pattern));

		// console.log(`until '${pattern.pattern}'`, parserState.targetString)

		const {
			targetString,
			index,
		} = parserState;

		const totalRemainingChars = targetString.length - index;

		let result = '';
		let count = 0;
		let nextState = parserState;

		while (true) {
			if (count > totalRemainingChars) {
				return updateParserError(
					nextState,
					'Unexpected EOF'
				);
			}

			const test = pattern(nextState);
			if (!test.isError) break;

			const currChar = nextState.targetString[nextState.index];
			result = result + currChar;

			nextState = updateParserState(
				nextState,
				nextState.index + 1,
			);

			count++;
		}

		if (require('../../../package.json').debug)
			console.log('>> Exiting until', JSON.stringify(pattern.pattern), 'matched', nextState.result)

		if (result.length < minLength) {
			return updateParserError(
				nextState,
				`Did not reach minimum length of ${minLength}`
			)
		}

		return updateParserResult(nextState, result);
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default until;