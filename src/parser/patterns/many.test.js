import { many, str } from '.';
import { defineTest } from '../../../test-helper';

test('matches a many instances of a pattern', () => {
	defineTest(
		many(str('s')),
		'sssssnake',
		5,
		['s', 's', 's', 's', 's']
	)
});

test('minimum instances to match', () => {
	defineTest(
		many(str('s'), 6),
		'sssssnake',
		5,
		undefined,
		"Expected 6 instances of 's' and only got 5"
	)
});