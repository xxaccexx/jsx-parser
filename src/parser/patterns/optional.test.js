import { optional, str } from '.'
import { defineTest } from '../../../test-helper';

const pattern = optional(str('something'));

test('matches a matching pattern', () => {
	defineTest(
		pattern,
		'something',
		9,
		'something'
	)
});

test('still matches, even if the pattern doesnt match', () => {
	defineTest(
		pattern,
		'soemthing',
		0,
	)
})