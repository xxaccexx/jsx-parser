import {
	updateParserError,
	updateParserIndex,
	updateParserResult,
} from '../state';

const many = (pattern, minimum = 0) => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		let nextState = parserState;
		let result = [];

		while (true) {
			if (nextState.index >= nextState.targetString.length) break;

			const test = pattern(nextState);
			if (test.isError) break;

			result.push(test.result);
			nextState = updateParserIndex(nextState, test.index);
		}

		if (result.length < minimum) return updateParserError(
			nextState,
			`Expected ${minimum} instances of '${pattern.pattern}' and only got ${result.length}`
		)

		return updateParserResult(
			updateParserIndex(
				parserState,
				nextState.index,
			),
			result
		);
	}

	parser.pattern = pattern.pattern;
	return parser;
}

export default many;