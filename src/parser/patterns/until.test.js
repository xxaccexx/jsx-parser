import { until, str, choice } from '.';
import { defineTest } from '../../../test-helper';

test('matches anything until specific pattern', () => {
	defineTest(
		until(str('!')),
		'something!',
		9,
		'something'
	)
})

test('fails if it reaches EOF', () => {
	defineTest(
		until(str('!')),
		'something',
		10,
		undefined,
		'Unexpected EOF'
	)
})

test('requires a minimum number of matches', () => {
	defineTest(
		until(str('!'), 1),
		'!something',
		0,
		undefined,
		'Did not reach minimum length of 1'
	)

	defineTest(
		until(str('!'), 1),
		's!omething',
		1,
		's'
	)
})

test('Match with priority on earlier patterns', () => {
	defineTest(
		choice(
			until(str('$')),
			until(str('<'))
		),
		'content$asd<',
		7,
		'content',
	)

	defineTest(
		choice(
			until(str('<')),
			until(str('$')),
		),
		'content$asd<',
		11,
		'content$asd',
	)
})