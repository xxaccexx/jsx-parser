import { defineTest } from '../../../test-helper';
import { choice, str } from '.';

const pattern = choice(
	str('<test>'),
	str('<text>'),
	str('<texas>')
)

test('matches test from one of the patterns', () => {
	defineTest(
		pattern,
		'<test>',
		6,
		'<test>'
	)
});

test('matches text from one of the patterns', () => {
	defineTest(
		pattern,
		'<text>',
		6,
		'<text>'
	)
});

test('matches texas from one of the patterns', () => {
	defineTest(
		pattern,
		'<texas>',
		7,
		'<texas>'
	)
});

test('doesnt match any of the patterns', () => {
	defineTest(
		pattern,
		'<tablet>',
		0,
		undefined,
		"Unexpected character '<' @ 0"
	)
})