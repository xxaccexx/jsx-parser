import { sequence, str } from '.';
import { defineTest } from '../../../test-helper';

const pattern = sequence(
	str('<'),
	str('asd'),
	str('>')
);

test('matches a sequence', () => {
	defineTest(
		pattern,
		'<asd>',
		5,
		['<', 'asd', '>']
	)
});

test('fails when the sequence doesnt match', () => {
	defineTest(
		pattern,
		'<asd >',
		4,
		undefined,
		"Expected '>' and got ' ' instead"
	)
})

test('fails when the sequence doesnt match', () => {
	defineTest(
		pattern,
		'<asd',
		4,
		undefined,
		"Unexpected EOF"
	)
})