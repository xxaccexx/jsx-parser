import {
	updateParserError,
	updateParserState,
	updateParserResult,
	updateParserIndex,
} from '../state';

const sequence = (...patterns) => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		let nextState = parserState;
		const result = [];

		for (let pattern of patterns) {
			const test = pattern(nextState);

			if (test.isError) return test;

			result.push(test.result);
			nextState = updateParserState(
				parserState,
				test.index
			);
		}

		return updateParserResult(
			updateParserIndex(
				parserState,
				nextState.index
			),
			result
		);
	}

	parser.pattern = patterns.map(p => p.pattern);

	return parser;
}

export default sequence;