import {
	updateParserError,
} from '../state';

const findMap = (arr, map, continueCondition) => {
	const mappedItems = [];

	arr.find((item, index, arr) => {
		const mapValue = map(item, index, arr);
		mappedItems.push(mapValue);
		return continueCondition(mapValue, index, arr);
	});

	return mappedItems;
}

const choice = (...patterns) => {
	const parser = parserState => {
		if (parserState.isError) return parserState;

		const results = findMap(
			patterns,
			pattern => pattern(parserState),
			nextState => !nextState.isError
		);

		const nextState = results.find(i => !i.isError);

		if (!!nextState) {
			return nextState;

		} else {
			return updateParserError(
				nextState,
				['', ...results.map(r => r.errorMsg.message || r.errorMsg), ''].join('\n-----\n')
			)
		}
	}

	parser.pattern = patterns.map(p => p.pattern);
	return parser;
}

export default choice;