import objectDiff from "./object-diff";

const ERRORS = {
	NO_KEY: () => new Error('Array items require a key for reconciliation'),
	DUP_KEY: (key) => new Error(`Array items require unqiue keys, saw '${key}' twice`)
}

const createKeyMap = (arr) => {
	const uniqueKeys = [];

	return arr.map((item, index) => {
		if (!item.key) throw ERRORS.NO_KEY();
		if (uniqueKeys.includes[item.key]) throw ERRORS.DUP_KEY(tem.key);

		uniqueKeys.push(item.key);
		return [item.key, index];
	});
}

const arrayDiff = (before, after) => {
	const diffs = [];

	const _keysBefore = createKeyMap(before);
	const keysBefore = Object.fromEntries(_keysBefore);

	const _keysAfter = createKeyMap(after);
	const keysAfter = Object.fromEntries(_keysAfter);

	//	removed, changed or sorted
	keysBefore.forEach(([key, index]) => {
		if (keysAfter[index] === undefined) diffs.push({ type: 'removed', index, keyBefore })
	})

	return diffs;
}

export default arrayDiff