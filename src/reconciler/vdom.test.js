import { createVDOM } from './vdom';

const VDOM = createVDOM();

test('examples', () => {
    const exampleTree = {
        $$type: 'ELEMENT',
        $$name: 'span',
        $$props: {
            onClick: () => console.log('test'),
            class: 'link',
        },
        $$children: [
            {
                $$type: 'CONTENT',
                $$content: 'Hello, World!',
            }
        ]
    }

    VDOM.applyTree(exampleTree);

    // expect(true).toEqual(false);
})