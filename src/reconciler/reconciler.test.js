import { TYPES } from "../parser/types";
import {
    renderContent,
    renderElement,
    renderExpression,
    renderNode,
} from '../cli-renderer/cli-renderer';

const before = {
    $$name: 'p',
    $$exp: [15],
    props: { },
    children: [
        {
            $$type: TYPES.CONTENT,
            content: 'You have clicked ',
        },
        {
            $$type: TYPES.EXPRESSION,
            $$exp: 0,
        },
        {
            $$type: TYPES.CONTENT,
            content: ' times.'
        }
    ]
}

const after = {
    $$name: 'p',
    $$exp: [16],
    props: { },
    children: [
        {
            $$type: TYPES.CONTENT,
            content: 'You have clicked ',
        },
        {
            $$type: 'expression',
            $$exp: 0,
        },
        {
            $$type: TYPES.CONTENT,
            content: ' times.'
        }
    ]
}

test('render content', () => {
    renderContent({
        $$type: TYPES.CONTENT,
        content: 'Example content'
    })
})

test('render expression', () => {
    renderExpression({
        $$type: TYPES.EXPRESSION,
        $$exp: 'Example expression value'
    })
})

test('render element', () => {
    renderElement({
        $$type: TYPES.ELEMENT,
        $$name: 'span',
        props: { class: 'link btn' },

    })
})

test('render element with children', () => {
    renderElement({
        $$type: TYPES.ELEMENT,
        $$name: 'span',
        props: { class: 'link btn' },
        children: [
            {
                $$type: TYPES.CONTENT,
                content: 'child content'
            }
        ]
    })
})

// test('render component', () => {
//     renderElement({
//         $$type: TYPES.ELEMENT,
//         $$name: 'Spanner',
//         $$fn: (props) => ({ $$type: TYPES.CONTENT, content: 'child content' }),
//         props: { class: 'link btn' },
//     })
// })
