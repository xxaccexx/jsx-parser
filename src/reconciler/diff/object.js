const objectDiff = (before, after) => {
	const diffs = [];

	const _before = Object.entries(before);
	const _after = Object.entries(after);

	//	removed or changed
	_before.forEach(([prop]) => {
		if (after[prop] === undefined) diffs.push({ type: 'removed', prop, oldValue: before[prop], newValue: undefined });
		else if (after[prop] !== before[prop]) diffs.push({ type: 'changed', prop, oldValue: before[prop], newValue: after[prop] });
	})

	//	added
	_after.forEach(([prop]) => {
		if (before[prop] === undefined) diffs.push({ type: 'added', prop, oldValue: undefined, newValue: after[prop] });
	})

	//	should be comprehensive with a loop.
	return diffs
}

export default objectDiff;