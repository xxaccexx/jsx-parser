import arrayDiff from './array';

test('always returns an array', () => {
	const a = [];
	const b = [];

	expect(arrayDiff(a, b)).toEqual([]);
})

test('reports missing indexes', () => {
	const a = [{ key: 'a' }, { key: 'b' }];
	const b = [{ key: 'a' }];

	expect(arrayDiff(a, b)).toEqual([{ type: 'removed', index: -1, oldValue: { key: 'b' }, newValue: undefined }]);
})

test('reports reordered indexes', () => {
	const a = [{ key: 'a' }, { key: 'b' }];
	const b = [{ key: 'b' }, { key: 'a' }];

	expect(arrayDiff(a, b)).toEqual([
		{ type: 'sorted', index: 1, oldValue: { key: 'a' }, newValue: { key: 'a' } },
		{ type: 'sorted', index: 0, oldValue: { key: 'b' }, newValue: { key: 'b' } }
	]);
})

test('reports added indexes', () => {
	const a = [{ key: 'a' }];
	const b = [{ key: 'a' }, { key: 'b' }];

	expect(arrayDiff(a, b)).toEqual([
		{ type: 'added', index: 1, oldValue: undefined, newValue: { key: 'b' } },
	]);
})

test('throws when missing keys', () => {
	const a = [{ value: 'a' }];
	const b = [{ value: 'a' }];

	expect(() => arrayDiff(a, b)).toThrow('Array items require a key for reconciliation');
})

test('throws with duplicate keys', () => {
	const a = [{ key: 'a' }, { key: 'a' }];
	const b = [{ key: 'a' }];

	expect(() => arrayDiff(a, b)).toThrow("Array items require unqiue keys, saw 'a' more than once");
})

test('handles all three', () => {
	const a = [{ key: 'remove me' }, { key: 'move me' }];
	const b = [{ key: 'move me' }, { key: 'add me' }];

	expect(arrayDiff(a, b)).toEqual([
		{ type: 'removed', index: -1, oldValue: { key: 'remove me' }, newValue: undefined },
		{ type: 'sorted', index: 0, oldValue: { key: 'move me' }, newValue: { key: 'move me' } },
		{ type: 'added', index: 1, oldValue: undefined, newValue: { key: 'add me' } },
	])
})