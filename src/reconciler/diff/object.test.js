import objectDiff from "./object";


test('will always return an array', () => {
	const a = {};
	const b = { diff: 'something' };

	expect(objectDiff(a, a)).toEqual([]);
})

test('report removed props', () => {
	const b = {};
	const a = { diff: 'something' };

	expect(objectDiff(a, b)).toEqual([{ type: 'removed', prop: 'diff', oldValue: 'something', newValue: undefined }]);
})

test('report changed props', () => {
	const a = { diff: 'something' };
	const b = { diff: 'else' };

	expect(objectDiff(a, b)).toEqual([{ type: 'changed', prop: 'diff', oldValue: 'something', newValue: 'else' }]);
})

test('report added props', () => {
	const a = {};
	const b = { diff: 'something' };

	expect(objectDiff(a, b)).toEqual([{ type: 'added', prop: 'diff', oldValue: undefined, newValue: 'something' }]);
})