import objectDiff from "./object";

const ERRORS = {
	NO_KEY: () => new Error('Array items require a key for reconciliation'),
	DUP_KEY: (key) => new Error(`Array items require unqiue keys, saw '${key}' more than once`)
}

//	returns a list of unique keys and a map for the values
//	also throws if there are missing or duplicate keys
const createKeyMap = (arr) => {
	const map = {};
	const keys = [];

	arr.forEach((item, index) => {
		if (!item.key) throw ERRORS.NO_KEY();
		if (map[item.key]) throw ERRORS.DUP_KEY(item.key);

		map[item.key] = item;
		keys.push(item.key);
	})

	return {
		keys,
		map
	}
}

const arrayDiff = (before, after) => {
	const diffs = [];

	const {
		map: beforeMap,
		keys: beforeKeys,
	} = createKeyMap(before);

	const {
		map: afterMap,
		keys: afterKeys,
	} = createKeyMap(after);

	//	removed or sorted
	beforeKeys.forEach((key, index) => {
		if (!afterMap[key]) diffs.push({ type: 'removed', index: -1, oldValue: beforeMap[key], newValue: undefined });
		else if (index !== afterKeys.indexOf(key)) diffs.push({ type: 'sorted', index: afterKeys.indexOf(key), oldValue: beforeMap[key], newValue: beforeMap[key] });
	});

	//	added
	afterKeys.forEach((key, index) => {
		if (!beforeKeys.includes(key)) diffs.push({ type: 'added', index, oldValue: undefined, newValue: afterMap[key] });
	})

	return diffs;
}

export default arrayDiff