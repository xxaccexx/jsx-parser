const basicComparison = (oldValue, newValue) => oldValue != newValue;

const createState = (id, initValue, onChange, shouldChange = basicComparison) => {
    if (!id) throw new TypeError('Cannot create a ');

    let currValue = initValue ?? undefined;
          
    return {
        get state() { return currValue },
        set state(nextValue) {
            if (shouldChange(currValue, nextValue)) {
                if (typeof onChange === 'function') onChange(currValue, nextValue);
                currValue = nextValue;
            }
        }
    }
}