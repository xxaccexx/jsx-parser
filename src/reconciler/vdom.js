import { TYPES } from '../parser/types';
import { objectDiff } from './diff'

export const createVDOM = (componentRegister) => {
    let vdom = undefined;

    ////    NODES
    //  create
    //  sorted
    //  remove

    ////    ATTRS
    //  create
    //  update
    //  remove

    ////    EVENTS
    //  create
    //  update
    //  remove

    ////    PROPERTIES
    //  create
    //  update
    //  remove


    const handleNodeCreation = (treeNode, vdomNode = {}) => {
        switch (treeNode.$$type) {
            case TYPES.ELEMENT: {
                if (vdomNode.$$elem) {} //  remove existing element
                //  add new element
                break;
            }
            
            case TYPES.CONTENT: {
                if (vdomNode.$$elem) {} //  remove exiting text node
                //  add new text node
                break;
            }

            //  expression

            //  component
        }
    }


    const traverse = (treeNode, vdomNode = {}) => {
        const childNodes = [];

        if (treeNode.$$type !== vdomNode.$$type) handleNodeCreation(treeNode, vdomNode);
        if (treeNode.$$name !== vdomNode.$$name) {} // rerender
        
        //  check props diff
        const propsDiff = objectDiff(vdomNode?.$$props ?? {}, treeNode.$$props);
        if (propsDiff.length) {} // rerender
        
        const newVDOMNode = {
            ...vdomNode,
            ...treeNode,
        }

        console.log('new node', newVDOMNode);
    }

    const applyTree = (tree) => {
        //  get vdom diffs
        const vdomDiffs = traverse(tree, vdom)
    }

    return {
        applyTree
    }
}