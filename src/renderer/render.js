const renderElement = (node, render) => {
	const el = document.createElement(node.$$tag);

	if (node.$$props) {
		const props = Object.entries(node.$$props);

		props.forEach(([prop, value]) => {
			if (prop.startsWith('@')) {
				const eventName = prop.substr(1);
				el.addEventListener(eventName, value);

			} else {
				el.setAttribute(prop, value);
			}
		})
	}

	if (node.$$children && Array.isArray(node.$$children)) {
		node.$$children.forEach(node => {
			el.appendChild(renderNode(node, render))
		})
	}

	return el;
}

const renderContent = (node, render) => {
	const el = document.createTextNode(node.$$value);
	return el;
}

const renderComponent = (node, render) => {
	const setState = (newState) => {
		node.$$state = newState;
		render();
	}

	const subtree = node.$$fn(node.$$props, node.$$state, setState);

	if (Array.isArray(subtree)) {
		return subtree.forEach(node => renderNode(node));
	} else {
		return renderNode(subtree)
	}
}

const renderNode = (node, render) => {
	switch (node.$$type) {
		case 'component': return renderComponent(node, render);
		case 'element': return renderElement(node, render);
		case 'content': return renderContent(node, render);
	}
}

const render = (componentTree, target) => {
	if (!(target instanceof HTMLElement)) throw new TypeError('target is not an HTMLElement');

	let parent = target;

	const reRender = async () => {
		await Promise.resolve();

		const root = parent.cloneNode();
		const subtree = renderNode(componentTree, reRender);
		root.appendChild(subtree);

		parent.parentElement.insertBefore(root, parent);
		parent.remove();
		parent = root;
	}

	reRender();
}

export default render;