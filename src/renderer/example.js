export default {
	$$type: 'component',
	$$props: {
		something: 'this is an external value',
	},
	$$state: {
		value: 2,
	},
	$$fn: (props, state, setState) => {
		return {
			$$type: 'element',
			$$tag: 'div',
			$$props: {
				class: 'input-field'
			},
			$$children: [
				{
					$$type: 'element',
					$$tag: 'label',
					$$props: {
						class: 'input-label',
						value: state.value,
					},
					$$children: [
						{
							$$type: 'content',
							$$value: 'Input value ',
						},
						{
							$$type: 'content',
							$$value: props.something,
						}
					]
				},
				{
					$$type: 'element',
					$$tag: 'input',
					$$props: {
						class: 'input',
						type: 'text',
						value: state.value,
					},
				},
				{
					$$type: 'element',
					$$tag: 'button',
					$$props: {
						class: 'increment',
						'@click': () => setState({ ...state, value: state.value + 1 }),
						'@change': (e) => setState({ ...state, value: e.target.value }),
					},
					$$children: [
						{
							$$type: 'content',
							$$value: '+'
						}
					]
				}
			]
		}
	},
}